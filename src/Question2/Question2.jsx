import React from "react";
import "./Question2.css";

class Question2 extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      isLoaded: false,
      categories: [],
      error: ''
    };
  }

  data = [
    ["0.122584", "0.785882", "0.954039", "0.353008", "1", "2", "0.954039", "0.353008"],
  ];

  componentDidMount() {
    fetch("https://api.publicapis.org/categories")
      .then(res => res.json())
      .then(
        (categories) => {
          this.setState({
            isLoaded: true,
            categories: categories
          });
        },
        (error) => {
          this.setState({
            isLoaded: true,
            error
          });
        }
      )
  }

  onFilterChange() {
    var input, filter, div, td, i, inputValue;
    input = document.getElementById('filterInput');
    filter = input.value.toUpperCase();
    div = document.getElementById("categoriesRow");
    td = div.getElementsByTagName('td');

    // Loop through all list items, and hide those who don't match the search query
    for (i = 0; i < td.length; i++) {
      inputValue = td[i].textContent;
      console.log(inputValue);
      if (inputValue.toUpperCase().indexOf(filter) > -1) {
        td[i].style.display = "";
      } else {
        td[i].style.display = "none";
      }
    }
  }

  render() {
    return (
      <table>
        <tbody>
        <tr>
          <th>Categories</th>
          <th>Filter: <input type="text" id="filterInput" onKeyUp={this.onFilterChange}/></th>
        </tr>
          {
            this.state.isLoaded ?
              <div id="categoriesRow">
                { this.state.categories.map(item => (
                  <tr key={item}>
                    <td key={item}>{item}</td>
                  </tr>
                ))}
              </div>
              : <tr />
          }
        </tbody>
    </table>
    );
  }
}

export default Question2;
