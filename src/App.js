import React from "react";
import { BrowserRouter, Switch, Route } from "react-router-dom";
import Question1 from "./Question1/Question1";
import Question2 from "./Question2/Question2";

class App extends React.Component {
  render() {
    return (
      <BrowserRouter>
        <Switch>
          <Route exact path="/" component={Question1} />
          <Route exact path="/q2" component={Question2} />
        </Switch>
      </BrowserRouter>
    );
  }
}

export default App;

