import React from "react";
import "./Question1.css";

class Question1 extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      input: 0,
      result: ''
    };

    this.onInputChange = this.onInputChange.bind(this);
    this.onOptionChange = this.onOptionChange.bind(this);
    this.isPrime = this.isPrime.bind(this);
    this.isFibonacci = this.isFibonacci.bind(this);
    this.isSquare = this.isSquare.bind(this);
  }

  onInputChange(event) {
    var inputValue = event.target.value;
    if (inputValue < 0) {
        inputValue = 1
    }
    this.setState({input: Math.round(inputValue)});
    const selected = document.getElementById("options").value;

    if(selected === 'isPrime') {
        this.isPrime(inputValue);
    } else {
        this.isFibonacci(inputValue);
    }
  }

  isPrime (num) {
    if (num === '1') {
        return this.setState({result: 'false'});
    } else if (num === '2') {
        return this.setState({result: 'true'});
    } else {
        for(var i = 2; i < num; i++) {
            if(num % i === 0) {
                return this.setState({result: 'false'});
            }
        }
    }
    return this.setState({result: 'true'});
  }

  isSquare(n) {
    return n > 0 && Math.sqrt(n) % 1 === 0;
  }

  isFibonacci(num) {
    if (this.isSquare(5*(num*num)-4) || this.isSquare(5*(num*num)+4)) {
        return this.setState({result: 'false'});
    } else { return this.setState({result: 'false'}) }
  }

  onOptionChange() {
    const selected = document.getElementById("options").value;

    if(selected === 'isPrime') {
        this.isPrime(this.state.input);
    } else {
        this.isFibonacci(this.state.input);
    }
  }

  render() {
    return (
        <div className="Question1">
            <div className="column1" id="column">
                <input type="number" min="0" onChange={this.onInputChange} value={this.state.input}/>
            </div>
            <div className="column2" id="column">
            <select id="options" onChange={this.onOptionChange}>
                <option value="isPrime">isPrime</option>
                <option value="isFibonacci">isFibonacci</option>
            </select>
            </div>
            <div className="column3" id="column">
                <span>{this.state.result}</span>
            </div>
        </div>
    );
  }
}

export default Question1;
